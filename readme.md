## Description of the task

- First, i have started by creating a Dockerfile, it consist of the flowing operations:
  - using php-apache:7.1 as a base image to be able to host this php project
  - installing system packages and PHP extensions
  - copying vhost.conf configuration file to /etc/apache2/sites-available which is an apache conf file
  - installing composer latest version, i have chosen to include it in the image because it might be needed later

- second, i have created a docker-compose file, i have decieded to not include a build phase in docker-compose in order not to      rebuild a new image each time we invoke docker-compose down or docker compose up, the file consist of the flowing services:
  - app service: this is the backend of the application which uses the image built by the docker file, this container works on port 805 and has a bind mount of all the files of the project which i will explain its purpose later.
  - db service: which is based on mariadb:10.2.8, this service works on 3306, it reads its environment variables from .env file and it has a named volume.
  - both services works on happs-network. 

- to let developers do their changes without the need to rebuild the image i have created a bind mount volume which bind all projcet files to the container, thus any developer can directly edit the code and the change will be reflected to the container, later if these changes needs to be pushed (as an image) the developer can either build a new image or build an image based on the current container using docker commit command.


## Installation & run

- After pulling the code from this repo Copy the .env.example file to .env
- inside the root folder build the laravel image using: docker build -t laravel .
- run the application using docker-compose --project-name happs up 
- After you install the database create a schema matching the informations in .env file in the projects' root folder.

You can change the next entries as it suits you before you build the db image. 

`DB_DATABASE=happs`

`DB_USERNAME=root`

`DB_PASSWORD=`
.

##### Install the apps' related packages with running: 

` docker-compose exec app composer update`

##### Create the tables and fill users table with data:

` docker-compose exec app php artisan migrate --seed` . 

### Run 

Once you setup apache then you should be able to open localhost:805 in your browser and login with:
`'test@app.com'` / `'password'` .

If you are able to login then everything is ok. 
